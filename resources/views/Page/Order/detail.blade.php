@extends('layouts.app')

@section('content')
<div class="page-container">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="">PO: 000732  <label class="label label-info">Waiting</label></h4>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-size-2">
					<div class="panel-heading"><h4 class="">PT. Purnama</h4></div>
					<div class="panel-body">
						<h3 class="no-margin">1200 l</h3>
						<p><b>Rp 9.024.000,-</b></p>
						<p>Tgl PO: 19-09-2017</p>
						<button class="btn btn-sm btn-primary">Edit PO</button>
						<button class="btn btn-sm btn-info">Ganti Status</button>
					</div>
				</div>
				
				
				<div class="panel panel-size-2">
					<div class="panel-heading"><h4 class="">Jadwal Pengiriman</h4></div>
					<div class="panel-body">
						<p>Tgl PO: 19-09-2017</p>
						<button class="btn btn-sm btn-primary">Ganti Jadwal</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<h4 class="">Konfirmasi Pengiriman <span class="badge badge-danger">3</span></h4>
				<div class="panel panel-size-3" style="height: 445px;
					overflow-y: scroll;
					background: transparent;">
					<!-- <div class="panel-body " > -->
						<!-- <div class="row"> -->
							<!-- <div class="col-sm-12"> -->
								<div class="panel bg-danger-lighter  panel-size-1">
									<div class="panel-heading"><span class="badge badge-danger">konfirmasi tidak diterima</span> <span class="pull-right bold">19-09-2017 16:35</span></div>
									<div class="panel-body">
										<p>No. Konfirmasi: 0000120123</p>
										<p>( pesan konfirmasi )Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus ...</p>
										
									</div>
								</div>
								<div class="panel bg-complete-lighter  panel-size-1 ">
									<div class="panel-heading"><span class="badge badge-danger">pending</span> <span class="pull-right bold">19-09-2017 16:35</span></div>
									<div class="panel-body">
										<p>No. Konfirmasi: 0000120123</p>
										<p>( pesan konfirmasi )Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus ...</p>
										<button class="btn btn-sm btn-warning">Check</button>
									</div>
								</div>
								<div class="panel bg-success-lighter  panel-size-1 no-margin">
									<div class="panel-heading"><span class="badge badge-danger">Konfirmasi diterima</span> <span class="pull-right bold">19-09-2017 16:35</span></div>
									<div class="panel-body">
										<p>No. Konfirmasi: 0000120123</p>
										<p>( pesan konfirmasi )Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus ...</p>
										<button class="btn btn-sm btn-danger">Batal</button>
									</div>
								</div>
							<!-- </div> -->
						<!-- </div> -->
					<!-- </div> -->
				</div>
			</div>
			<div class="col-sm-5">
				<div class="panel panel-success panel-size-3">
						
					<div class="panel-heading"><button class="btn btn-sm btn-primary pull-right">Edit Kapten</button><p class=""><b>Kapten 1 </b><span>O875566443</span></p></div>
					<div class="panel-body chats">
						<div class="row chat chat-guest">
							<div class="inner pull-right">Bagai mana, apakah sudah sampai tujuan ? <br><small>09:33</small></div>
						</div>
						<div class="row chat">
							<div class="inner"><b>Kapten 1: </b><span> maaf.. masih belum, ada kendala teknis di kapal kami.<br><small>09:43</small></div></span>
						</div>
						<div class="row chat chat-guest">
							<div class="inner pull-right">Kira kira jam brapa sampai di tujuan?<br><small>09:48</small></div>
						</div>
						<div class="row chat">
							<div class="inner"><b>Kapten 1: </b><span> Kira kira jam 1 siang<br><small>09:55</small></div></span>
						</div>
					</div>
					<div class="panel-footer" style="position: absolute;
				    width: 100%;
				    bottom: 0;
					">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Type here .." name="">
							<div class="input-group-addon">img</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection