@extends('layouts.app')

@section('content')
<div class="page-container">
	<div class="container">
		<div class="row">
		<div class="col-md-12">
			<h3>Order List <a href="{{ route('order.create') }}" class="btn btn-success pull-right">Buat Order Baru</a></h3>

		</div>
		<div class="col-md-12">

			<div class="panel panel-default">
				<div class="panel-heading"></div>
				<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>No. PO</th>
									<th>Kapten</th>
									<th>Pemesan</th>
									<th>Jumlah</th>
									<th class="text-right">Total</th>
									<th class="text-center">Pengiriman</th>
									<th class="text-center">Tgl PO</th>
									<th class="text-center">Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><a href="{{ route('order.show', ['id'=>'0008813']) }}">0008813</a></td>
									<td>Kapten 1</td>
									<td>PT. Purnama</td>
									<td>1200 l</td>
									<td class="text-right">Rp. 9.024.000,-</td>
									<td class="text-center">Semarang, 28-10-2017</td>
									<td class="text-center">19-9-2017</td>
									<td class="text-center"><span class="label label-info">Waiting</span></td>
								</tr>	
								<tr>
									<td><a href="{{ route('order.show', ['id'=>'0008813']) }}">0008813</a></td>
									<td>Kapten 1</td>
									<td>PT. Purnama</td>
									<td>1200 l</td>
									<td class="text-right">Rp. 9.024.000,-</td>
									<td class="text-center">Semarang, 28-10-2017</td>
									<td class="text-center">19-9-2017</td>
									<td class="text-center"><span class="label label-success">On Progress</span></td>
								</tr>	
								<tr>
									<td><a href="{{ route('order.show', ['id'=>'0008813']) }}">0008813</a></td>
									<td>Kapten 1</td>
									<td>PT. Purnama</td>
									<td>1200 l</td>
									<td class="text-right">Rp. 9.024.000,-</td>
									<td class="text-center">Semarang, 28-10-2017</td>
									<td class="text-center">19-9-2017</td>
									<td class="text-center"><span class="label label-danger">Pending</span></td>
								</tr>
								<tr>
									<td><a href="{{ route('order.show', ['id'=>'0008813']) }}">0008813</a></td>
									<td>Kapten 1</td>
									<td>PT. Purnama</td>
									<td>1200 l</td>
									<td class="text-right">Rp. 9.024.000,-</td>
									<td class="text-center">Semarang, 28-10-2017</td>
									<td class="text-center">19-9-2017</td>
									<td class="text-center"><span class="label label-default">success</span></td>
								</tr>								
							</tbody>
						</table>	
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
@endsection