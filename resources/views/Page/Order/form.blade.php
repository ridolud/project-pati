@extends('layouts.app')

@section('content')
<div class="page-container"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ url('/order/create') ? 'Buat Order' : 'Edit Order' }}
                    <span class="pull-right">
                        <a href="{{ route('order.index') }}">Back to list</a>
                    </span>
                </div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="">
                        {{--!! isset($data) ? method_field('put') : '' !!--}}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">No. PO</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($data) ? $data->name : old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Pilih Kapten</label>

                            <div class="col-md-6">
                                <!-- <input id="email" type="email" class="form-control" name="email" value="{{ isset($data) ? $data->email : old('email') }}" required> -->
                                <select class="form-control">
                                    <option>Kapten 1</option>
                                    <option>Kapten 2</option>
                                    <option>Kapten 3</option>
                                </select>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Pemesan</label>

                            <div class="col-md-6">
                                <input id="password" type="text" class="form-control" value="{{ old('password') }}" name="password" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Jumlah</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                <input id="password" type="number" class="form-control" value="{{ old('password') }}" name="password" required>
                                <div class="input-group-addon">liter</div>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Total</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                <div class="input-group-addon">Rp </div>
                                <input id="password" type="number" class="form-control" value="{{ old('password') }}" name="password" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Tujuan Pengiriman</label>

                            <div class="col-md-6">
                                <input id="password" type="text" class="form-control" value="{{ old('password') }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Jadwal Pengiriman</label>
                            <div class="col-md-4">
                                <input id="password" type="text" class="form-control" placeholder="HH-BB-TTTT" value="{{ old('password') }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <input id="password" type="text" class="form-control" placeholder="JJ:MM" value="{{ old('password') }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <a type="" class="btn btn-primary">
                                    Simpan
                                </a>
                            </div>
                        </div>
                    </form>
                     @if(isset($data))
                        <form action="{{ route('user.destroy',$data->id) }}" method="POST">
                            {!! method_field('delete') !!}
                            {{ csrf_field() }}
                            <a type="submit" class="btn btn-danger">Delete</a>
                        </form>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
