@extends('layouts.app')

@section('content')
<div class="page-container">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">User List</div>
				<div class="panle-body">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Role</th>
									<th>Create at</th>
								</tr>
							</thead>
							<tbody>
								@foreach($data as $user)
									<tr>
										<td><a href="{{ route('user.show',$user->id) }}">{{ $user->name }}</a></td>
										<td>{{ $user->email }}</td>
										<td>Kapten</td>
										<td>{{ $user->created_at }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection