@extends('layouts.app')

@section('content')
<div class="container-fuild page-container full-height">
  <div id="calendar_month" class="full-height">
    <div class="calendar month">
      <div class="options">
        <div class="months-drager drager">
          <div class="months pull-left" id="months">
            <div class="month">
              <a href="#" class="month-selector " data-month="Jan">Jan</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Feb">Feb</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Mar</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Apr</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Mei</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Jun</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Jul</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Aug</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector active has-event" data-month="Mar">Sep</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Oct</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Nov</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">Dec</a>
            </div>
          </div>
          <button data-toggle="collapse" href="#sumary" class="btn btn-sm btn-info pull-right">Show sumary</button>
          <h4 class="semi-bold date pull-right" style="margin-right: 20px;" id="currentDate">September 2017, 19 Tuesday</h4>
        </div>
        
        <!-- <div class="drager week-dragger">
          <div class="weeks-wrapper" id="weeks-wrapper"></div>
        </div> -->
        </div>
          <div id="calendar" class="calendar-container">
            <div class="view month-view">
              <div class="tble" id="viewTableHead">
                <div class="thead">
                <div class="tcell" data-day="2017-09-17">
                  <div class="weekday">Sunday</div>
                </div>
                <div class="tcell" data-day="2017-09-18">
                  <div class="weekday">Monday</div>
                </div><div class="tcell" data-day="2017-09-19">
                  <div class="weekday">Tuesday</div>
                </div><div class="tcell" data-day="2017-09-20">
                  <div class="weekday">Wednesday</div>
                </div>
                <div class="tcell" data-day="2017-09-21">
                  <div class="weekday">Thursday</div>
                </div>
                <div class="tcell" data-day="2017-09-22">
                  <div class="weekday">Friday</div>
                </div>
                <div class="tcell" data-day="2017-09-23">
                  <div class="weekday">Saturday</div>
                </div>
              </div>
            </div>
            <div class="grid"><div class="tble" id="monthGrid">
              <div class="trow">
                <div class="tcell not" data-date="2017-09-17">
                  <div class="month-date">27</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell not" data-date="2017-09-18">
                  <div class="month-date">28</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell not" data-date="2017-09-19">
                  <div class="month-date">29</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell not" data-date="2017-09-20">
                  <div class="month-date">30</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell not" data-date="2017-09-21">
                  <div class="month-date">31</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-22">
                  <div class="month-date">1</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-23">
                  <div class="month-date">2</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div class="event-container bg-complete-lighter " style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title">1. PO: 0000233</div>
                        </div>
                      </div>
                      <div class="event-container bg-complete-lighter " style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title">2. PO: 0000235</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="trow">
                <div class="tcell" data-date="2017-09-17">
                  <div class="month-date">3</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-18">
                  <div class="month-date">4</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell " data-date="2017-09-19">
                  <div class="month-date ">5</div>
                  <div class="cell-inner">
                    <div class="holder">
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-20">
                  <div class="month-date">6</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div id="testActiveEvent" class="event-container bg-complete-lighter" style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title">3. PO: 0000533</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-21">
                  <div class="month-date">7</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-22">
                  <div class="month-date">8</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-23">
                  <div class="month-date">9</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
              </div>
              <div class="trow">
                <div class="tcell" data-date="2017-09-17">
                  <div class="month-date">10</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-18">
                  <div class="month-date">11</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell " data-date="2017-09-19">
                  <div class="month-date ">12</div>
                  <div class="cell-inner">
                    <div class="holder">
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-20">
                  <div class="month-date">13</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-21">
                  <div class="month-date">14</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-22">
                  <div class="month-date">15</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div class="event-container bg-complete-lighter " style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title"  data-toggle="collapse" href="#event">4. PO: 0000221</div>
                        </div>
                      </div>
                      <div class="event-container bg-complete-lighter " style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title">5. PO: 0000239</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-23">
                  <div class="month-date">16</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
              </div>
              <div class="trow">
                <div class="tcell" data-date="2017-09-17">
                  <div class="month-date">17</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-18">
                  <div class="month-date">18</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div class="event-container bg-complete-lighter " style="width:100%" data-index="2" data-starttime="2017-09-18T02:00:00+07:00" data-endtime="2017-09-18T06:00:00+07:00" id="ca_15056748002" data-id="ca_15056748002">
                        <div class="event-inner">
                          <div class="event-title">6. PO: 0000509</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tcell current-date active" data-date="2017-09-19">
                  <div class="month-date current-date active">19</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div class="event-container bg-success-lighter " style="width:100%" data-index="3" data-starttime="2017-09-19T02:00:00+07:00" data-endtime="2017-09-19T03:00:00+07:00" id="ca_15057612003" data-id="ca_15057612003">
                        <div class="event-inner">
                          <div class="event-title">7. PO: 0000221</div>
                        </div>
                      </div>
                      <div class="event-container bg-danger-lighter " style="width:100%" data-index="4" data-starttime="2017-09-19T05:00:00+07:00" data-endtime="2017-09-19T06:00:00+07:00" id="ca_15057720004" data-id="ca_15057720004">
                        <div class="event-inner">
                          <div class="event-title">8. PO: 0000245</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-20">
                  <div class="month-date">20</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-21">
                  <div class="month-date">21</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-22">
                  <div class="month-date">22</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-23">
                  <div class="month-date">23</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
              </div>
              <div class="trow">
                <div class="tcell" data-date="2017-09-17">
                  <div class="month-date">24</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-18">
                  <div class="month-date">25</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell " data-date="2017-09-19">
                  <div class="month-date">26</div>
                  <div class="cell-inner">
                    <div class="holder">
                      
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-20">
                  <div class="month-date">27</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-21">
                  <div class="month-date">28</div>
                  <div class="cell-inner">
                    <div class="holder">
                      <div class="event-container bg-success-lighter " style="width:100%" data-index="3" data-starttime="2017-09-19T02:00:00+07:00" data-endtime="2017-09-19T03:00:00+07:00" id="ca_15057612003" data-id="ca_15057612003">
                        <div class="event-inner">
                          <div class="event-title">9. PO: 0000643</div>
                        </div>
                      </div>
                      <div class="event-container bg-success-lighter " style="width:100%" data-index="4" data-starttime="2017-09-19T05:00:00+07:00" data-endtime="2017-09-19T06:00:00+07:00" id="ca_15057720004" data-id="ca_15057720004">
                        <div class="event-inner">
                          <div class="event-title">10. PO: 0000245</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-22">
                  <div class="month-date">29</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
                <div class="tcell" data-date="2017-09-23">
                  <div class="month-date">30</div>
                  <div class="cell-inner">
                    <div class="holder"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@include('Page.Schedule.sumary')
@include('Page.Schedule.event')
@endsection