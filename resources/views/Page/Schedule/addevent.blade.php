<div class="collapse quickview-wrapper" id="addevent">
	<div class="inner">
		<div class="quickview-header">
			<div class="container">
			<h4 class=""><a data-toggle="collapse" href="#addevent" aria-hidden="true"><i class="glyphicon glyphicon-chevron-left"></i></a> September 2017, 20 Wednesday</h4>
			</div>
		</div>
		<div class="quickview-body">
		<div class="container">
		<div class="col-md-12">
			<form method="POST" action="" class="form-horizontal">
			    <input type="hidden" name="_token" value="jNqvn5mNoQmgTTcns2xzLvi6INzyJAdxsmUgNmpi">
			    <div class="form-group">
			            <input id="name" type="text" placeholder="No. PO" name="name" value="" required="required" autofocus="autofocus" class="form-control">
			    </div>
			    <div class="form-group">
			            <select class="form-control">
			                <option>Pilih Kapten 1</option>
			                <option>Kapten 2</option>
			                <option>Kapten 3</option>
			            </select>
			    </div>
			    <div class="form-group">
			            <input id="password" placeholder="Nama Pemesan" type="text" value="" name="password" required="required" class="form-control">
			    </div>
			    <div class="form-group">
			            <input id="password" placeholder="No.tlp Pemesan" type="text" value="" name="password" required="required" class="form-control">
			    </div>
			    <div class="form-group">
			            <div class="input-group">
			                <input id="password" placeholder="Jumlah Minyak" type="number" value="" name="password" required="required" class="form-control">
			                <div class="input-group-addon">liter</div>
			            </div>
			    </div>
			    <div class="form-group">
			            <div class="input-group">
			                <div class="input-group-addon">Rp </div>
			                <input id="password" type="number" value="" placeholder="Total Harga" name="password" required="required" class="form-control">
			            </div>
			    </div>
			    <div class="form-group">
			            <input id="" placeholder="Tujuan Pengiriman" type="text" value="" name="password" required="required" class="form-control">
			    </div>
			    <div class="form-group">
			        <label for="password" class="col-md-12 control-label">Jadwal Pengiriman</label>
			    </div>
			    <div class="row form-group">
			        <div class="col-sm-7">
			            <input id="password" type="text" placeholder="HH-BB-TTTT" value="20-09-2017" name="password" required="required" class="form-control">	
			        </div>
			        <div class="col-sm-5">
			            <input id="password" type="text" placeholder="JJ:MM" value="10:00" name="password" required="required" class="form-control">
			        </div>
			    </div>
			    <div class="form-group">
			            <div class="input-group">
			                <input id="password" placeholder="Estimasi Pengiriman" type="number" value="" name="password" required="required" class="form-control">
			                <div class="input-group-addon">Jam</div>
			            </div>
			    </div>
			    <div class="form-group">
			        <div class="col-md-6 col-md-offset-4"><a type="" class="btn btn-primary">
			                                    Simpan
			                                </a></div>
			    </div>
			</form>
			</div>
		</div>
		</div>
	</div>
</div>
