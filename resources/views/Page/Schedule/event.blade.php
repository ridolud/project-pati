<!-- <div class="modal fade" id="modalEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">2. PO:0008999 </h4>
        <div class="time-wrap small">
              06-09-2017 <span class="event-start-time">2:00 am</span> - <span class="event-end-time">3:00 am</span>
            </div>
      </div>
      <div class="modal-body">
        
            <p>Kapten : Kapten 1<br>
            Total liter : 1200 l<br>
            Tgl order : 28-08-2017<br>
            Tujuan pengiriman: Semarang
            </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Edit Order</button>
      </div>
    </div>
  </div>
</div> -->
<div class="collapse quickview-wrapper" id="event">
  <div class="inner">
    <div class="quickview-header">
      <div class="container">
      <h4 class=""><a data-toggle="collapse" href="#event" aria-hidden="true"><i class="glyphicon glyphicon-chevron-left"></i></a> PO: 0000872 <label class="label label-info">Complete</label></h4>
      </div>
    </div>
    <div class="quickview-body">
      <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Detail</a></li>
    <li role="presentation"><a href="#chat" aria-controls="chat" role="tab" data-toggle="tab">Chat</a></li>
  </ul>
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="detail">
      <div class="container">
            <div class="panel"><div class="panel-heading"><h4>PT. Purnama</h4></div> <div class="panel-body"><h3 class="no-margin">1200 l</h3> <p><b>Rp 9.024.000,-</b></p> <p>Tgl PO: 19-09-2017</p><p>Kapten 1 - O875566443
</p><p>Estimasi pengiriman: 3 jam</p> <button class="btn btn-sm btn-primary">Edit PO</button> <button class="btn btn-sm btn-info">Ganti Status</button></div></div>
            <div class="panel"><div class="panel-heading"><h4>Jadwal Pengiriman</h4></div> <div class="panel-body">
            <p>19-09-2017</p>
            <p>09:00 - 12:00</p> <button class="btn btn-sm btn-primary">Ganti Jadwal</button></div></div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane chat-view" id="chat"><div class="chat-inner" id="my-conversation">
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Hello Kapten
                     <br><small style="
    position: absolute;
    top: 5px;
    left: -36px;
">19:06</small>
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Iya
                    <br>
                    <small style="
                        position: absolute;
                        top: 5px;
                        right: -36px;
                    ">19:06</small>
                  </div>
                </div>
                <!-- END From Them Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                   Bagaimana? 
                    <br>
                    <small style="
                        position: absolute;
                        top: 5px;
                        left: -36px;
                    ">19:00</small>
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Me Message  !-->
                <div class="message clearfix">
                  <div class="chat-bubble from-me">
                    Apakah sudah sampai tujuan ? 
                     <br>
                    <small style="
                        position: absolute;
                        top: 5px;
                        left: -36px;
                    ">19:06</small>
                  </div>
                </div>
                <!-- END From Me Message  !-->
                <!-- BEGIN From Them Message  !-->
                <div class="message clearfix">
                  <div class="profile-img-wrapper m-t-5 inline">
                    <img class="col-top" width="30" height="30" src="assets/img/profiles/avatar_small.jpg" alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg">
                  </div>
                  <div class="chat-bubble from-them">
                    Belum masih ada kendala
                    <br>
                    <small style="
                        position: absolute;
                        top: 5px;
                        right: -36px;
                    ">19:06</small>
                  </div>
                </div>
                <!-- END From Them Message  !-->
              </div>
              <div class="form-group" style="    position: absolute;
    bottom: 0;">
                <div class="input-group"><input id="password" type="" value="" name="password" required="required" class="form-control"> <div class="input-group-addon">Send</div></div>
              </div>
              </div>
  </div>
    </div>
  </div>
</div>