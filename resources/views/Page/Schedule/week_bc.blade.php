@extends('layouts.app')

@section('content')
<div class="container-fuild page-container full-height">
  <div id="calendar_month" class="full-height">
    <div class="calendar week">
      <div class="options">
        <div class="months-drager drager">
          <div class="months pull-left" id="months" style="max-width: 400px;overflow-y: auto;">
            <div class="month">
              <a href="#" class="month-selector " data-month="Jan">1</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Feb">2</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">3</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">4</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">5</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">6</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">7</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">8</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector" data-month="Mar">9</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">10</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">11</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">12</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">13</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">14</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">13</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">14</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">15</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">16</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">17</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">18</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector active has-event" data-month="Mar">19</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">20</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">21</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">22</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">23</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">24</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">25</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">26</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">27</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">28</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">29</a>
            </div>
            <div class="month">
              <a href="#" class="month-selector " data-month="Mar">30</a>
            </div>
          </div>
          <button data-toggle="collapse" href="#sumary" class="btn btn-sm btn-info pull-right">Show sumary</button>
          <button data-toggle="collapse" href="#addevent" class="btn btn-sm btn-info pull-right" style="margin-right: 5px">Tambah Jadwal</button>
          <h4 class="semi-bold date pull-right" style="margin-right: 20px;" id="currentDate">September 2017, 19 Tuesday</h4>
        </div>
        </div>
          <div id="calendar" class="calendar-container">
            <div class="view week-view">
              <div class="allday-cell"></div>
              <div class="tble" id="viewTableHead">
                <div class="thead"><div class="tcell" data-day="2017-09-17">
                  <div class="weekdate">17</div>
                  <div class="weekday">Sunday</div>
                </div><div class="tcell" data-day="2017-09-18">
                  <div class="weekdate">18</div>
                  <div class="weekday">Monday</div>
                  <div class="event-bubble bg-complete-lighter"></div>
                </div><div class="tcell active" data-day="2017-09-19">
                  <div class="weekdate">19</div>
                  <div class="weekday">Tuesday</div>
                </div><div class="tcell" data-day="2017-09-20">
                  <div class="weekdate">20</div>
                  <div class="weekday">Wednesday</div>
                </div>
                <div class="tcell" data-day="2017-09-21">
                  <div class="weekdate">21</div>
                  <div class="weekday">Thursday</div>
                </div>
                <div class="tcell" data-day="2017-09-22">
                  <div class="weekdate">22</div>
                  <div class="weekday">Friday</div>
                </div>
                <div class="tcell" data-day="2017-09-23">
                  <div class="weekdate">23</div>
                  <div class="weekday">Saturday</div>
                </div>
                </div>
              </div>
            <div class="grid">
              <div class="time-slot-wrapper" id="time-slots">
                <div class="time-slot">
                  <span>8:00</span>
                </div>
                <div class="time-slot">
                  <span>9:00</span>
                </div>
                <div class="time-slot">
                  <span>10:00</span>
                </div>
                <div class="time-slot">
                  <span>10:00</span>
                </div>
                <div class="time-slot">
                  <span>11:00</span>
                </div>
                <div class="time-slot">
                  <span>12:00</span>
                </div>
                <div class="time-slot">
                  <span>13:00</span>
                </div>
                <div class="time-slot">
                  <span>14:00</span>
                </div>
                <div class="time-slot">
                  <span>15:00</span>
                </div>
                <div class="time-slot">
                  <span>16:00</span>
                </div>
              </div>
              <div class="tble" id="weekGrid">
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"><div class="event-container bg-complete-lighter " data-event-duration="240" data-index="2" data-starttime="2017-09-25T02:00:00+07:00" data-endtime="2017-09-25T06:00:00+07:00" id="ca_15062796002" data-id="ca_15062796002" data-row="2" data-cell="1" style="height:320px;"><div class="event-inner"><div class="event-title">1. PO: 0000321</div><div class="time-wrap"><span class="event-start-time">09:00</span> - <span class="event-end-time">12:00</span></div></div><div class="resizable-handle"></div></div></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00">
                        
                      </div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"><div class="event-container bg-success-lighter " data-event-duration="120" data-index="3" data-starttime="2017-09-26T02:00:00+07:00" data-endtime="2017-09-26T04:00:00+07:00" id="ca_15063660003" data-id="ca_15063660003" data-row="2" data-cell="2" style="height:160px;"><div class="event-inner"><div class="event-title">2. PO: 000321</div><div class="time-wrap"><span class="event-start-time">10:00</span> - <span class="event-end-time">11:00</span></div></div><div class="resizable-handle"></div></div></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00">
                        <span href="#" style=" 
    position: absolute;
    padding: 9px 25px;
    display: none;
" ><i class="glyphicon glyphicon-plus"></i> Tambah Jadwal</span>
                      </div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"><div class="event-container bg-danger-lighter " data-event-duration="120" data-index="3" data-starttime="2017-09-26T02:00:00+07:00" data-endtime="2017-09-26T04:00:00+07:00" id="ca_15063660003" data-id="ca_15063660003" data-row="2" data-cell="2" style="height:160px;"><div class="event-inner"><div class="event-title">3. PO: 000331</div><div class="time-wrap"><span class="event-start-time">12:00</span> - <span class="event-end-time">14:00</span></div></div><div class="resizable-handle"></div></div></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
                <div class="trow">
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell active">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                   <div class="tcell">
                      <div class="cell-inner" data-time-slot="0:00"></div>
                      <div class="cell-inner" data-time-slot="0:30"></div>
                   </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@include('Page.Schedule.sumary')
@include('Page.Schedule.event')
@include('Page.Schedule.addevent')
@endsection