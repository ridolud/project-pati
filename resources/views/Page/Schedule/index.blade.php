@extends('layouts.app')

@section('content')
	<div class="container-fuild page-container full-height">
		<month
            prev="prev"
            next="next"
            now="{{ $currentDate }}"
        ></month>
	</div>	
@endsection