@extends('layouts.app')

@section('content')
  <div class="container-fuild page-container full-height">
    <week
            prev="prev"
            next="next"
            now="{{ $currentDate }}"
        ></week>
  </div>  
@endsection