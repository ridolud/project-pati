@extends('layouts.app')

@section('content')
<div class="page-container">
<div class="container">
    <div class="row">
        <div class="col-md-2 text-center">
                    <a href="{{ route('order.create') }}" class="btn btn-block btn-lg btn-success" style="margin-top: 20px">Buat Order Baru</a>
                    <p style="margin-top: 20px;"><a href="" class="">Cetak Laporan Penjualan Hari Ini</a></p>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default panel-size-1">
                <!-- <div class="alert alert-success">
                            You are logged in!
                </div> -->
                <div class="panel-heading">
                    <h4 class="panel-title">Harga minyak hari ini</h4>
                    <div class="panel-option"><a href="#">update</a></div>
                </div>

                <div class="panel-body full-height">
                    <h2 class="text-center" style="margin-top: 10px;"><span class="text-info">Rp. 7.230,-</span>  <small>/ltr</small><h2>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default panel-size-1">
                <!-- <div class="alert alert-success">
                            You are logged in!
                </div> -->
                <div class="panel-heading">
                    <h4 class="panel-title">Rangkuman hari ini</h4>
                    <!-- <div class="panel-option"><a href="#">update</a></div> -->
                </div>

                <div class="panel-body full-height">
                    <!-- <h2 class="text-center"><span class="text-success">Rp. 7.230,-</span>  <small>/ltr</small><h2> -->
                    <div class="row">
                        <div class="col-sm-3  ">
                            <h2 class=" no-margin text-success">5</h3>
                            <p>Jadwal pengiriman</p>
                            <!-- <small><a href="#">lihat</a></small> -->
                        </div>
                        <div class="col-sm-3  ">
                            <h2 class=" no-margin text-success">8</h3>
                            <p>Order baru</p>
                            <!-- <small><a href="#">lihat</a></small> -->
                        </div>
                        <div class="col-sm-3  ">
                            <h2 class=" no-margin text-danger">2</h3>
                            <p>Order pending</p>
                            <!-- <small><a href="#">lihat</a></small> -->
                        </div>
                        <div class="col-sm-3 text-center ">
                            <h2 class=" no-margin">46000 l</h3>
                            <p>Minyak tersedia</p>
                            <!-- <small><a href="#">lihat</a></small> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel"></div>
    </div>
</div>
</div>
@endsection
