<div class="header">
    <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Project Pati
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <!-- <ul class="nav navbar-nav">
                        &nbsp;
                    </ul> -->

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification (<b>2</b>)</a>
                              <ul class="dropdown-menu notify-drop">
                                <div class="notify-drop-title">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">Bildirimler (<b>2</b>)</div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu."><i class="fa fa-dot-circle-o"></i></a></div>
                                    </div>
                                </div>
                                <!-- end notify title -->
                                <!-- notify content -->
                                <div class="drop-content">
                                    <li>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> yorumladı. <a href="">Çicek bahçeleri...</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                                        
                                        <hr>
                                        <p class="time">Şimdi</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> yorumladı. <a href="">Çicek bahçeleri...</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                                        <p>Lorem ipsum sit dolor amet consilium.</p>
                                        <p class="time">1 Saat önce</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> yorumladı. <a href="">Çicek bahçeleri...</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                                        <p>Lorem ipsum sit dolor amet consilium.</p>
                                        <p class="time">29 Dakika önce</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> yorumladı. <a href="">Çicek bahçeleri...</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                                        <p>Lorem ipsum sit dolor amet consilium.</p>
                                        <p class="time">Dün 13:18</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-md-3 col-sm-3 col-xs-3"><div class="notify-img"><img src="http://placehold.it/45x45" alt=""></div></div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 pd-l0"><a href="">Ahmet</a> yorumladı. <a href="">Çicek bahçeleri...</a> <a href="" class="rIcon"><i class="fa fa-dot-circle-o"></i></a>
                                        <p>Lorem ipsum sit dolor amet consilium.</p>
                                        <p class="time">2 Hafta önce</p>
                                        </div>
                                    </li>
                                </div>
                                <div class="notify-drop-footer text-center">
                                    <a href=""><i class="fa fa-eye"></i> Tümünü Göster</a>
                                </div>
                              </ul>
                            </li>
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="">
                                <a href="#" class="" data-toggle="" role="" aria-expanded="">
                                    {{ Auth::user()->name }}
                                </a>
                            </li>
                            <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                        @endif
                    </ul>
                </div>
            </div>
    </nav>
    <nav class="navbar navbar-default navbar-static-top header-menu">
            <div class="container">
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('home') }}">Dashboard</a></li>
                        <li>
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false">Schedule <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('schedule.month') }}">Month</a></li>
                                <li><a href="{{ route('schedule.week',['year'=>'2017', 'month'=>'09', 'date'=>'19']) }}">Week</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('order.index') }}">Order</a></li>
                        <li><a href="#">Report</a></li>
                        <li><a href="{{ route('user.index') }}">User</a></li>
                        <li><a href="#">Setting</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Notification (<b>2</b>)</a>
                              <ul class="dropdown-menu notify-drop">
                                <div class="notify-drop-title">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">Order complete (<b>2</b>)</div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right"><a href="" class="rIcon allRead" data-tooltip="tooltip" data-placement="bottom" title="tümü okundu."><i class="fa fa-dot-circle-o"></i></a></div>
                                    </div>
                                </div>
                                <!-- end notify title -->
                                <!-- notify content -->
                                <div class="drop-content">
                                    <li>
                                        <div class="col-xs-12">
                                            <p class="bold">PO: 000623 - Konfirmasi</p>
                                            <p>Kapten 1 telah megirim konfirmasi pengiriman <a href="{{ route('order.show',['id'=>'009965']) }}">cek konfirmasi</a></p>
                                        
                                        <hr>
                                        <p class="time">1 minute ago</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-xs-12">
                                            <p class="bold">PO: 000623 - Pesan</p>
                                            <p>Kapten 2 - Ada kendala di kapal kami mungkin ini akan menghabat pengiriman .. <a href="{{ route('order.show',['id'=>'009965']) }}">baca</a></p>
                                        
                                        <hr>
                                        <p class="time">30 minute ago</p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-xs-12">
                                            <p class="bold">PO: 000623 - Complete</p>
                                            <p>Admin 1 telah merubah status menjadi complete <a href="{{ route('order.show',['id'=>'009965']) }}">lihat order</a></p>
                                        
                                        <hr>
                                        <p class="time">1 hour ago</p>
                                        </div>
                                        <div class="col-xs-12">
                                            <p class="bold">PO: 000623 - Perubahan Jadwal</p>
                                            <p>Admin 3 telah merubah jadwal pengiriman <a href="{{ route('order.show',['id'=>'009965']) }}">lihat order</a></p>
                                        
                                        <hr>
                                        <p class="time">yesterday</p>
                                        </div>
                                    </li>
                                </div>
                                <div class="notify-drop-footer text-center">
                                    <a href=""><i class="fa fa-eye"></i> Lihat Semua</a>
                                </div>
                              </ul>
                            </li>
                            </ul>
                </div>
            </div>
    </nav>
</div>