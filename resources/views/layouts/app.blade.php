<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Project Pati</title>

    <!-- Styles -->
    <link href="{{ asset('css/calendar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('layouts.header')
        @include('alert::bootstrap')
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        
    $(function() {
    $('.tcell').dblclick(function(event){
            // return window.location.href = "{{ route('schedule.week',['year'=>'2017', 'month'=>'09', 'date'=>'19']) }}";
            var x = event.target.className;


            // if(x.indexOf( "month-date" ) !== -1){
            //     return alert(x);
            // }
            if(x.indexOf( "event-title" ) !== -1 ){
                $('#addevent').collapse('hide');
                $('#event').collapse('show');
            }else{
              window.location.href = "{{ route('schedule.week',['year'=>'2017', 'month'=>'09', 'date'=>'19']) }}";
            }
            
        });

    $('.quickview-body .event-container [data-toggle="collapse"]').click(function(){
        $('#testActiveEvent').toggleClass('active');
    });
    $('.week .cell-inner').hover(function(){
        $(this).children('span').toggle();
        $(this).children('span').click(function(){
            $('#addevent').collapse('show');
        });
    });
    
    });

    </script>
</body>
</html>
