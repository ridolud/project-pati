
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');
window.moment = require('moment');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('month', require('./components/schedule/Month.vue'));
Vue.component('week', require('./components/schedule/week.vue'));
Vue.component('dayofmonth', require('./components/schedule/Dayofmonth.vue'));

const app = new Vue({
    el: '#app',
    methods: {
            moment(param) {
                return moment(param);
            }
        },
});
