<?php
//testing
Route::get('/demo/schedule', function(){
	return view('Page.Schedule.index_bc');
});
Route::get('/demo/schedule/week', function(){
	return view('Page.Schedule.week_bc');
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/welcome', function() {
	return view('welcome');
});
Route::get('/fire', function(){
	\Event::fire(new App\Events\TestEvent(1));
	return 'Event fired. Go to /show to observe.';
});

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


//Must Auth
Route::group(['middleware' => 'auth'], function() {

	//Dashbord / order
	Route::get('/', 'HomeController@index')->name('home');

	//Users
	Route::resource('user', 'UserController',['except' => ['edit']]);

	//Schedule
	Route::get('/schedule/{year?}/{month?}/', 'ScheduleController@month')->name('schedule.month');
	Route::get('/schedule/{year?}/{month?}/{date?}', 'ScheduleController@week')->name('schedule.week');

	//Orders
	Route::resource('order', 'OrderController',['except' => ['edit']]);
});

