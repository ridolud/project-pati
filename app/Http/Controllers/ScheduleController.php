<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Carbon\Carbon;

class ScheduleController extends Controller
{

	private $current;

    public function __construct()
    {
    	$this->current = Carbon::now();
    }

    public function month($year = null, $month = null)
    {
    	$this->current = Carbon::createFromDate($year, $month);
    	return view('Page.Schedule.index')->with('currentDate', $this->current);
    }
    public function week($year = null, $month = null, $date=null)
    {
        $this->current = Carbon::createFromDate($year, $month, $date);
        return view('Page.Schedule.week')->with('currentDate', $this->current);
    }
}
